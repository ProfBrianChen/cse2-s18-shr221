import java.util.Scanner;
public class Hw06 {
public static void main(String[] args){
  Scanner myScanner = new Scanner(System.in);
  //Course number entry
  System.out.println("Enter course number  ");  //scanner to input course number
    while(!myScanner.hasNextInt()){ //runs while loop if course number is not integer
     System.out.println("Please enter integer "); //prints scanner
      myScanner.next(); //scanner
    }
  int courseNum = myScanner.nextInt(); //initalizes course number as integer
 
  //days class meets entry
  System.out.println("Enter number of days class meets  "); //scanner to input meeting amount
  while(!myScanner.hasNextInt()){ //if input is not integer run while loop
     System.out.println("Invalid type, enter integer"); //prints scanner
     myScanner.next(); //scanner
  }
  int meetAmt = myScanner.nextInt(); //sets as integer
 
  //Students in class
  System.out.println("Enter number of students in class  "); //input for students in class
  while(!myScanner.hasNextInt()){ //if input is not integer
  System.out.println("Invalid type, enter integer"); //prints scanner
  myScanner.next(); //scanner
  }
  int numStudents = myScanner.nextInt(); //sets number of students as integer
 
  
  //start time
  System.out.println("enter the start time of the class with no colon (ie-7:00 would be 700) "); //scanner print statement
  while(!myScanner.hasNextInt()){ //run while loop if input is not integer
    System.out.println("Invalid type, enter integer");
    myScanner.next();
  }
  int startTime = myScanner.nextInt(); //sets an integer
  //Department name
  System.out.println("Enter department name  "); //scanner input
  
  while(!myScanner.hasNext()){ //if input is not scanner
    System.out.println("Invalid type, please enter string"); //if input is not string
    myScanner.next(); //scanner
    }
  String deptName = myScanner.next(); //sets dept name as string
  
   System.out.println("Enter Instructor's first Name:  "); //prompt user to enter instructorName
  
  while (!myScanner.hasNext()) { //if string is not entered properly
     System.out.println("Invalid type, enter string"); //if input is not string
     myScanner.next();
   }
  String firstName = myScanner.next(); //sets first name as string
  
  System.out.println("Enter instructor's last name  "); //enter last name
  while (!myScanner.hasNext()){ //if input is not string
    System.out.println("Invalid type, enter string"); //if input is not string
    myScanner.next();
   
}
  
String lastName = myScanner.next(); //sets last name as string
  
  System.out.println("Course number is  " + courseNum); //prints course number
  System.out.println("Course meets  " + meetAmt + "  times a week"); //prints meeting amount a week
  System.out.println("there are  " + numStudents + "  students in the class"); //prints students
  System.out.println("Class starts at  " + startTime); //prints start time
  System.out.println("Department name is  " + deptName); //prints dept name
  System.out.println("Instructor's name is  "  + firstName + " " + lastName); //prints teacher name
  
   
  }
}
