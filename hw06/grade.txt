Grade:   99/100

Comments:

A) Does the code compile?  How can any compiler errors be resolved?
Code compile correctly.
B) If the code compiles, does the code run properly?  What kinds of input cause a runtime error?
Code runs without RE, but it accepts integers for class name and professor.
C) How can any runtime errors be resolved?
N/A
D) What topics should the student study in order to avoid the errors they made in this homework?
N/A
E) Other comments:
to check if an integer was used instead of a string word, use hasNextInt and hasNextDouble, if they return false then use hasNext to accept the input.
