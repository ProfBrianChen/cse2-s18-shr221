import java.util.Random;
import java.util.Arrays;
public class RobotCity{
   public static void main(String[]args){
     //call other methods 
    int[][] population = buildCity();
     for(int i = 0; i<5; i++){
      display(population);
       System.out.println();
    int numberOfRobots = (int)(Math.random() * 5);
     invade(population, numberOfRobots);
     display(population);
      System.out.println();
     update(population);
     display(population);
       System.out.println();
     }
     }
   
  
    public static int[][] buildCity() { //city population 
  //blocks 
   int y = (int)(Math.random()*(16-10))+10; //coordinates west to east
   int x = (int)(Math.random()*(16-10))+10; //coordinates north to south 
   int[][] cityArray = new int[y][x]; //do the for loops follow the order y,x?
   for(int i=0; i<cityArray.length; i++){ //rows 
     for(int j = 0; j<cityArray[i].length; j++){
      cityArray[i][j] =(int)(Math.random()*(1000-100)) + 100; //array is random int from 100-999
 }
 }
 return cityArray;     
  }
  
public static void display (int[][] population){
   for(int i = 0; i<population.length; i++){
       for(int j = 0; j<population[i].length; j++){
         System.out.printf("%6d", population[i][j]);
       }
     System.out.println();
}
}
public static void invade(int[][] city, int numberOfRobots){
for(int i = 0; i<numberOfRobots; i++){
  int y = (int)(Math.random()*(city.length)); //random row
  int x = (int)(Math.random()*(city[0].length)); //lands somewhere in row
  if(city[y][x] >= 0){
    city[y][x]*=-1;
  }
}
}
public static void update(int[][] city){
  for(int i = 0; i< city.length; i++){
    int[] currentRow = city[i];
    if(currentRow[currentRow.length - 1] <= 0) {
      currentRow[currentRow.length - 1] *= -1;
    }
    for(int j = currentRow.length-2; j >= 0; j--){
      if(currentRow[j] < 0){
        currentRow[j+1]*= -1;
        currentRow[j]*=-1;
      }
      
    }
  }
}  
  
  
  
}
