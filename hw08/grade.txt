Grade:  80/100

Comments:
A) Does the code compile?  How can any compiler errors be resolved?
Both programs compile correctly.
B) If the code compiles, does the code run properly?  What kinds of input cause a runtime error?
Remove elements- Only deletes last number in array instead of at index. 
Cse2Linear- does not scramble array, does not allow for user to enter another number. 
C) How can any runtime errors be resolved?
Need another scanner for end of Cse2Linear
D) What topics should the student study in order to avoid the errors they made in this homework?
Scanners, Arrays.
E) Other comments:
Overall, nice job.