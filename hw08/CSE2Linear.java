import java.util.Scanner;
import java.util.Random;
import java.util.Arrays;// imports Scanner in order to have users input values
public class CSE2Linear//main method required for every Java Program 
{
  public static void main(String[]args){
    System.out.println("enter 15 ints from 0-100");
    Scanner myScan = new Scanner(System.in); //scanner declaration
    int count = 0; //initializes counter as 0
    int[] arrayOne = new int[15]; //array of length 15 for 15 entered ints
    for(count = 0; count<15; count++){
      if(myScan.hasNextInt()){ //if input is int
        arrayOne[count] = myScan.nextInt(); //sets array as scanner
        if(arrayOne[count] < 0 || arrayOne[count] > 100){ //checks if array is in range
          System.out.println("error. input is out of range"); 
        }
        else{ //continues if input is in range
          if(count > 0){ 
          if(arrayOne[count] < (arrayOne[count-1])){ //checks if entry is greater than previous entry
            System.out.println("error. second entry must be greater than previous entry");
            System.exit(0);
        }
            
          }
          //count ++; //increment counter
        }
      }
      else{ //if input is not int
        System.out.println("error. entry must be int");
        System.exit(0); //exits program
      }
    }
    String num = Arrays.toString(arrayOne);
    System.out.println("sorted ");
    System.out.println(num); //converts array to string
    
    System.out.println("enter a grade to search for- ");
    if(myScan.hasNextInt()){ //if entry is int
      int grades = myScan.nextInt(); //array is int
     // BinarySearch(arrayOne, grade); //
      //Scramble(arrayOne); //calls scramble method
      
      //String num = Arrays.toString(arrayOne);
      //if(myScan.hasNextInt()){
        linearSearch(arrayOne, grades); //calls linear search method
      System.out.println("Scrambled: " + Scramble(arrayOne)); //calls scramble method
      System.out.println("enter another grade to search for");
      }
      else{ //if entry is not an int
        System.out.println("entry was not int ");
        System.exit(0);
      }
      
    
    
    
  }
  public static void linearSearch(int[] arrayOne, int grades){ //method for search
    for(int i = 0; i<arrayOne.length; i++){ //for loop while i is less than array length
      if(arrayOne[i] == grades){ //if number entered is in array
        System.out.println("your entry " + grades + " was found in " + (i+1) + " iterations");
      }
      else if (arrayOne[i] != grades && grades == (arrayOne.length - 1)){ //if number is not in array or less than previous entry
        System.out.println("your entry " + grades + " was not found in " + (i+1) + "iterations");
        break; //break out of for loop
      }
    }
  }
  public static int[] Scramble(int[] arrayOne){ //scramble method
   for(int i = 0; i<arrayOne.length; i++){ //while i is less than array length
     int grades = (int)(Math.random()*arrayOne.length); //randomize grade array
     int storage = arrayOne[i]; //store array one temporarily
     while(grades != i){ //while grades array is not equal to i
       arrayOne[i] = arrayOne[grades]; //set array with i equal to array w grades
       arrayOne[grades] = storage;
       break;
     }
     
   } 
    return arrayOne;  //return arrayOne to main method
  }
}
    
  
  
  
  
  

