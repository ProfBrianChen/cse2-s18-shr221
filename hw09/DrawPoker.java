
import java.util.Arrays;
import java.util.Random;
public class DrawPoker {
  public static void main(String[] args) {
    int[] deck = new int[52]; //creates array 52 ints long for deck of cards
    for (int i = 0; i < 52; i++) { //while i is less than array length
      deck[i] = i; //members of deck=i for each value
    }
    shuffle(deck); //calls on method  to shuffle deck

    int[] playerOne = new int[5]; //assigns player one array of 5 long for 5 cards
    int[] playerTwo = new int[5]; //assigns player two array of 5 long for 5 cards
    for (int i = 0; i < 5; i++) { //for int i less than array 5
      playerOne[i] = deck[2*i]; //player one's members assigned this val
      playerTwo[i] = deck[2*i + 1]; //player two's members assigned this val
    }

    
    System.out.println("Player 1's Hand: "); //print player one's hands
    for (int i : playerOne) { 
      System.out.print(i + " ");
    }
    System.out.println("\n"); //adds space imbetween for formatting

    System.out.println("Player 2's Hand: "); //print player two's hands
    for (int i : playerTwo) {
      System.out.print(i + " ");
    }


    //Determine Which Hand Wins
    
    int playerOnePoints = 0; //initializes points
    int playerTwoPoints = 0;

    if ( Pair(playerOne) ) { //checks hands if it is pair by calling method
      playerOnePoints += 1; //adds one to points for pair
    }

    if (Pair(playerTwo) ) { //checks hands if it is pair
      playerTwoPoints += 1; //adds one to points for pair
    }

    if (ThreeOfAKind(playerOne) ) { //calls method to check for three of a kind then adds points
      playerOnePoints += 2;
    }

    if (ThreeOfAKind(playerTwo) ) { //calls method to check for three of a kind then adds points
      playerTwoPoints += 2;
    }
    
    if (Flush(playerOne) ) { //checks for flush by calling on method
      playerOnePoints += 4;
    }

    if (Flush(playerTwo) ) { //checks for flush by calling on method
      playerTwoPoints += 4;
    }

    if (fullHouse(playerOne) ) { //calls method to check for full house
      playerOnePoints += 8;
    }

    if (fullHouse(playerTwo) ) {//calls method to check for full house
      playerTwoPoints += 8;
    }
    
    if (playerOnePoints == playerTwoPoints) { //if cards are equal, check highest card
      if ( compHighCard(playerOne, playerTwo) ) { //calls method
        playerOnePoints += 1; //if player one has higher cards
      }
      else {
        playerTwoPoints += 1; //if player two has higher cards
      }
    }

    //Final Resilt
    if (playerOnePoints > playerTwoPoints) { //if player one has more points
      System.out.println("\nPlayer 1 Wins!");
    }
    else {
      System.out.println("\nPlayer 2 Wins!"); //else player two wins
    }

  }

  public static void shuffle(int[] myVal) { //shuffle method, returns void
    for (int i = 0; i < myVal.length; i++) { //while i is less than deck length
      int randomVal = (int) (Math.random() * myVal.length); //randomly shuffles vals in input length
      int temp = myVal[i]; //sets as temp
      myVal[i] = myVal[randomVal]; 
      myVal[randomVal] = temp; //reassigns to temp
    }
  }
  public static boolean Pair(int[] hand) { //boolean method
    for (int i = 1; i < hand.length; i++) {
      if (hand[i] % 13 == hand[0] % 13 ) { //two of same card
        return true;
      }
    } 
    return false;
  }
  public static boolean ThreeOfAKind(int[] hand) {
    for (int i = 1; i < hand.length; i++) {
      for (int k = i + 1; k < hand.length; k++) {
        if (hand[0] % 13 == hand[i] % 13 && hand[0] % 13 == hand[k] % 13 ) { //three of same card
          return true;
        }
      }
    } //end of for loops
    return false;
  }

  public static boolean Flush(int[] hand) {
    int counter = 0;
    for (int i = 1; i < hand.length; i++) {
      if (hand[0] / 13 == hand[i] / 13) { 
        counter += 1;
      }
    }
    if (counter == 4) {
      return true;
    }
    return false;
  }

  public static boolean fullHouse(int[] hand) {
    if (Pair(hand) && ThreeOfAKind(hand) ) { //if hand has both cases
      return true;
    }
    return false;
  }

  public static boolean compHighCard(int[] hand1, int[] hand2) {
    int maxHand1 = hand1[0];
    for (int i = 0; i < hand1.length; i++) {
      if (hand1[i] > maxHand1) {
        maxHand1 = hand1[i];
      }
    }
    int maxHand2 = hand2[0];
    for (int i = 0; i < hand2.length; i++) {
      if (hand2[i] > maxHand2) {
        maxHand2 = hand2[i];
      }
    }
    if (maxHand1 > maxHand2) {
      return true;
    }
    return false;
  }
}
