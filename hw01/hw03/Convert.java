import java.util.Scanner;
public class Convert{ 
public static void main(String[] args) {
Scanner myScanner = new Scanner( System.in );
System.out.print("Enter the affected area in acres in the form xxxx:" ); //input for affected area
double affectedArea = myScanner.nextDouble(); //sets as double
System.out.print("Enter the rainfall in the affected areas in inches in the form xx:" ); //input for rainfall
double rainFall = myScanner.nextDouble(); //sets as double
double acre_inches = (rainFall * affectedArea); //calculation for acre inches
double cubic_miles = (acre_inches / 40550400); //conversion to cubic miles
System.out.println("Total rain fall in cubic miles is" + cubic_miles); //print statement
}
}
