Grade:  100/100

Comments:

A) Does the code compile?  How can any compiler errors be resolved?
Both programs compile.
B) If the code compiles, does the code run properly?  What kinds of input cause a runtime error?
Convert and Pyramid both run correctly (think about inputs though, what if the user puts in a negative or a string)
C) How can any runtime errors be resolved?
N/A
D) What topics should the student study in order to avoid the errors they made in this homework?
N/A
E) Other comments:
Good job. Try to use better spacing and comments next time to make your code easier to read.
