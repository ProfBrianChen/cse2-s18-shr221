import java.util.Scanner; //import scanner
public class Area{ //start of every code
static double rectHeight; //declares each part of area as static so they can be recalled in multiple methods
static double rectWidth;
static String redoRect; //redo in case user inputs wrong string
static double triangBase;
static double triangHeight;
static String redoTriang;
static double circRadius;
static String redoCirc;
  
  public static void main (String[]args){ //main method to set what area equals
    double area; //declares area for each shape
    Scanner myScanner = new Scanner(System.in); //sets scanner
    System.out.println("enter rectangle, triangle, or circle"); //tells user what to enter
    String shape = myScanner.nextLine(); //accepts user input
    int verifyInput = valCheck(shape); //verifyInput calls on method valCheck to verify user input
    while(verifyInput==4){ //if user enters wrong word
      System.out.println("out of range. input for rectangle, triangle, or cirlce");
      shape = myScanner.next(); //sets equal to redo scanner
      verifyInput = valCheck(shape);     
    }
  if(verifyInput == 1){ //calls this method if input = 1 for area of rect
   area = rectArea(rectHeight, rectWidth); //for calling method when input = 1
    System.out.println("rectangle area is " + area);
  }
   else if(verifyInput == 2){ //calls this method if input = 2 for triangle area
     area = triangArea(triangBase, triangHeight);
     System.out.println("area of triangle is " + area);
   }
   else if(verifyInput == 3){ //calls method when input = 3
     area = circArea(circRadius);
     System.out.println("area of circle is " + area);
   }
  }
  public static int valCheck(String value){ //method to check input for doubles
    Scanner myScanner = new Scanner(System.in);
   String rect = "rectangle"; //sets what strings are equal to when they are typed in
   String triang = "triangle";
   String circ = "circle";
   if(value.equals(rect)){ //if input equals rectangle
     System.out.println("enter height for rectangle");
     while(!(myScanner.hasNextDouble())){ //if user does not enter double, redo the input message
       redoRect = myScanner.next();
       System.out.println("height must be double");
     }
     rectHeight = myScanner.nextDouble(); //sets height equal to double
     System.out.println("enter width for rectangle");
     while(!(myScanner.hasNextDouble())){ //if user does not enter double
       redoRect = myScanner.next(); //re do input message
       System.out.println("width must be double");
   } 
   rectWidth = myScanner.nextDouble(); //sets equal to scanner for input
     return 1; //return input = 1
  }
    else if (value.equals(triang)){ //if input equals triangle
      System.out.println("enter height of triangle"); //enter height
      while(!myScanner.hasNextDouble()){ //if input is not double
       redoTriang = myScanner.next(); 
        System.out.println("height must be double"); //redo input message
    }
    triangHeight = myScanner.nextDouble(); //sets equal to scanner
    
     System.out.println("enter base of triangle "); //enter base of triangle
     while(!myScanner.hasNextDouble()){ //if input is not double
       redoTriang = myScanner.next();   
       System.out.println("base must be double"); //redo input message
     }
     triangBase = myScanner.nextDouble(); 
      return 2; //return 2
    }
    else if (value.equals(circ)){ //if string equals circle
      System.out.println("enter radius of circle"); //enter radius
      while(!myScanner.hasNextDouble()){ //if input is not double
       redoCirc = myScanner.next();  //redo scanner message
       System.out.println("radius must be double");
    }
    circRadius = myScanner.nextDouble();
      return 3; //return 3
    }
      else{ //tells user to enter correct input
        return 4;
      }
           }
    public static double rectArea(double Width, double Height){ //method for rect area
      return Width*Height; //return area as w*h
    }
    public static double triangArea(double Base, double HeightTwo){ //method for tri area
      return (Base*HeightTwo)/2; //return area 
    }
    public static double circArea(double radius){ //method for circle area
      return 3.14*radius*radius; //return area
    }
           }


    
    
    
    
    
    
    
    