import java.util.Scanner;// imports Scanner in order to have users input values
public class encrypted_x//main method required for every Java Program 
{
    public static void main (String[]args)
    {
      System.out.println ("enter positive integer from 0-100");
        Scanner myScanner = new Scanner(System.in);// declares an instance of the Scanner object and constructs it
        int input = 0;
        while (input==0){
          if(myScanner.hasNextInt()){
            int length = myScanner.nextInt();
            if(length>0 && length<100){
              for(int i = 0; i <= length; i++){
                for(int j = 0; j <= length; j++){
                 if(i==(length-j) || i==j){
                   System.out.print(" "); 
                 }
                  else{
                    System.out.print("!?");
                  }
                }
                System.out.println("");
                
                }
              }

            else{
              System.out.println("enter number between 0 and 100");
            }
          }
          else{
            System.out.println("input must be integer between 0 and 100");
            myScanner.next();
          }
        }
        
    }
}
